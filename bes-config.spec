Name:		bes-config
Version:	1.3
Release:	2
License:	GPL
Summary:	Bes config save and restore utilities.
Group:		System Environment/Base
Source:		%{name}.tar.bz2
Vendor:		University of Cambridge
Packager:	Bes Support <bes-support@ucs.cam.ac.uk>
Buildroot:	%{_tmppath}/%{name}-%{version}-build
BuildArch:	noarch
Requires:	openssh, openssl >= 0.9.5, coreutils, bash, mktemp, tar, bzip2
Requires:	diffutils, sed, grep
Prefix:		/usr

%description
The Bes config save and restore utilities (bes-configsave and bes-configrestore,
respectively) are used to save and restore, in a secure fashion, a list of files
and directories specified by the user in /etc/bes-configfiles on the local
machine to the managed installation service Bes.

%prep
%setup -n %{name}

%build

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
mkdir -p "%{buildroot}%{_bindir}"
cp bes-config* "%{buildroot}%{_bindir}/"

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%{_bindir}/bes-config*

%changelog
* Thu Aug 27 2015 Ben Harris <bjh21@cam.ac.uk>
- Change dependency from "ssh" to "openssh" since SLES 11 SP4 openssh doesn't
  provide ssh any more.
* Tue Aug 20 2013 Anton Altaparmakov <aia21@cam.ac.uk>
- Add "set -o pipefail" and error handling so that we detect when the password
  has been mistyped when creating the 3DES encrypted archive and abort.
* Thu Mar 08 2007 Anton Altaparmakov <aia21@cam.ac.uk>
- Fix a bug where tar would bomb out due to misparsing the command line in
  SLES 10.  Work around by rearranging the command line to tar.
* Fri Feb 17 2006 Anton Altaparmakov <aia21@cam.ac.uk>
- Add "-n" option to bes-config{save,restore} to disable the comparison to the
  previously saved state and to just do it without asking for confirmation.
* Tue Feb 14 2006 Anton Altaparmakov <aia21@cam.ac.uk>
- Wrote the initial spec file.
